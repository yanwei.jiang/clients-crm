<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void {
        parent::setUp();

        $this->client = Client::create([
            'company_name' => 'Test Company',
            'address' => '1 Test Street',
            'phone' => '021 999 999'
        ]);
        $this->apiUrl = '/api/clients/' . $this->client->id;
    }

    public function test_can_get_contacts_list()
    {
        $contacts = Contact::factory()->create(
            ['client_id' => $this->client->id]
        );
        $newClient = Client::factory()->create();
        $contacts->push(Contact::factory()->create(
            ['client_id' => $newClient->id]
        ));

        $this->getJson($this->apiUrl . '/contacts')
            ->assertOk()
            ->assertJsonCount(1)
            ->assertJsonStructure([
                [
                    'id',
                    'name',
                    'email',
                    'phone',
                    'type'
                ]
            ]);
    }
}
