<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ClientTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_get_clients_paginated_list()
    {
        $clients = Client::factory(2)->create();

        $this->getJson('/api/clients')
            ->assertOk()
            ->assertJson(function (AssertableJson $json) {
                $json->has('meta')
                    ->has('data', 2, function ($json) {
                        $json->whereAllType([
                            'id' => 'integer',
                            'company_name' => 'string',
                            'address' => 'string',
                            'phone' => 'string'
                        ])->etc();
                    })
                    ->etc();
            });
    }

    public function test_can_get_clients_filtered_by_keyword()
    {
        $client = Client::factory()->create([
            'company_name' => 'John Doe Company',
            'address' => '36 Test Street'
        ]);

        $this->getJson('/api/clients?keywords=john')
            ->assertOk()
            ->assertSee(['John Doe Company', '36 Test Street']);
    }

    public function test_data_are_valid_when_creating_a_client()
    {
        $data = [];
        $this->postJson('/api/clients', $data)
            ->assertStatus(422)
            ->assertJson(function (AssertableJson $json) use ($data) {
                $json->has('message')
                    ->has('errors', 3)
                    ->whereAllType([
                        'errors.company_name' => 'array',
                        'errors.address' => 'array',
                        'errors.phone' => 'array'
                    ]);
            });

        $data = [
            'company_name' => 'Test Company',
            'address' => '1 test street, test city',
        ];
        $this->postJson('/api/clients', $data)
            ->assertStatus(422)
            ->assertJsonValidationErrors(
                ['phone']
            )->assertJsonMissingValidationErrors(['company_name', 'address']);
    }

    public function test_can_create_a_client()
    {
        $this->withoutExceptionHandling();

        $data = [
            'company_name' => 'Test Company',
            'address' => '1 test street, test city',
            'phone' => '09 333 3333'
        ];

        $response = $this->handleValidationExceptions()
            ->postJson('/api/clients', $data);

        // Assert db record
        $this->assertDatabaseHas('clients', [
            'company_name' => 'Test Company'
        ])->assertDatabaseCount('clients', 1);

        $response->assertJsonMissingValidationErrors()
                ->assertCreated()
                ->assertJson($data);
    }

    public function test_can_show_a_client()
    {
        $client = Client::factory()->create([
            'company_name' => 'Test Company',
            'address' => '1 test street, test city'
        ]);

        $this->getJson('/api/clients/' . $client->id)
            ->assertOk()
            ->assertJson([
                'company_name' => 'Test Company',
                'address' => '1 test street, test city',
            ]);
    }

    public function test_can_update_a_client()
    {
        $client = Client::factory()->create([
            'company_name' => 'John Doe Company',
            'address' => '36 Test Street'
        ]);

        $this->putJson('/api/clients/' . $client->id, [
                'company_name' => 'Update Company',
                'address' => '1 Test Street'
            ])
            ->assertOk()
            ->assertJson(function (AssertableJson $json) use ($client) {
                $json->where('id', $client->id)
                    ->where('company_name', 'Update Company')
                    ->where('address', '1 Test Street')
                    ->etc();
            });
    }
}
