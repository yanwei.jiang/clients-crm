<?php

namespace App\Http\Controllers;

use App\Http\Resources\ClientResource;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keywords = $request->keywords;

        $clients = Client::when($keywords, function ($query, $keywords) {
                $query->where('company_name', 'like', "%$keywords%");
            })
            ->orderBy('company_name')
            ->paginate();

        return response()->json(
            resource_data(ClientResource::collection($clients))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'company_name' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);

        $client = Client::create($data);
        $resource = new ClientResource($client);

        return response()->json(
            resource_data($resource),
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        $resource = new ClientResource($client);

        return response()->json(
            resource_data($resource),
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $data = $this->validate($request, [
            'company_name' => 'sometimes|required',
            'address' => 'sometimes|required',
            'phone' => 'sometimes|required'
        ]);

        $client->update($data);
        $resource = new ClientResource($client);

        return response()->json(
            resource_data($resource)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
