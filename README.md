## Project setup
```
git clone git@gitlab.com:yanwei.jiang/clients-crm.git
cd clients-crm

cp .env.example .env
php artisan key:generate

composer install
npm install
```

## Run project
```
Set up database credentials in .env file
php artisan migrate
php artisan db:seed

php artisan serve (or use any other local server environment)
```

## Features And Highlights

- [ ] Use model factory and database seeder to seed fake data for quick development
- [ ] Use Laravel resources to add a transformation layer for api data
- [ ] Use implicit route model binding
- [ ] Add feature test for clients and contacts controller. To run the phpunit test,
run ```./vendor/bin/phpunit```
- [ ] Use Modular vuex store for managing states in different parts of the app

## Todos and Out of scope for this project
- [ ] Authentication
- [ ] Edit and add new contact for client
- [ ] Toast notification when user has successfully added or updated a client
- [ ] Better and unified styling across whole app
