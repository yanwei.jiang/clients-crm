import Vue from 'vue'
import VueRouter from 'vue-router'
import ClientsList from './pages/ClientsList'
import ClientDetail from './pages/ClientDetail'
import EditClient from './pages/EditClient'

Vue.use(VueRouter)

const routes = [
  { path: '/clients', name: 'clients', component: ClientsList, props: true },
  { path: '/clients/create', name: 'client_create', component: EditClient},
  { path: '/clients/:id', name: 'client_detail', component: ClientDetail, props: true },
  { path: '/clients/:id/edit', name: 'client_edit', component: EditClient},
  { path: '/', redirect: '/clients' },
]

export default new VueRouter({
  mode: 'history',
  routes
})
