import axios from 'axios';

export const apiClient = axios.create({
  // backend API url, please change if it's different
  // baseURL: 'http://localhost:8000',
  withCredentials: true,
});

export const fetchAllClients = (queryString = '') => {
  return apiClient.get('/api/clients' + queryString);
}

export const upsertClient = (clientId, data) => {
  return apiClient.post('/api/clients/' + clientId, data);
}

export const fetchClient = (clientId) => {
  return apiClient.get('/api/clients/' + clientId);
}

export const fetchClientContacts = (clientId) => {
  return apiClient.get('/api/clients/' + clientId + '/contacts');
}
