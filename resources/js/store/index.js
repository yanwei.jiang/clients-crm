import Vue from 'vue';
import Vuex from 'vuex';
import general from './modules/general';
import clients from './modules/clients';
import contacts from './modules/contacts';

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    general,
    clients,
    contacts,
  },
  strict: debug,
})
