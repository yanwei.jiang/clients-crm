import { fetchAllClients, fetchClient, upsertClient } from '../../api';

const state = () => ({
  clients: [],
  meta: {
    totalPages: 1,
    total: 0,
    perPage: 0,
    currentPage: 1,
  },
  currentClient: null,
})

const getters = {
    
}

const actions = {
    async getAllClients({ commit, dispatch }, queryString) {
      dispatch('general/setIsLoading', {}, {root:true})

      let response = await fetchAllClients(queryString);
      if (response.status === 200) {
        console.log('here')
        commit('setClients', response.data.data);
        commit('setMeta', response.data.meta);
      }
      
      dispatch('general/setIsLoaded', {}, {root:true});
    },
    async createOrUpdateClient({ commit,dispatch }, { clientId, data }) {
        let response = await upsertClient(clientId, data);
        return response;
    },
    async getClient({ commit }, clientId) {
        let response = await fetchClient(clientId);
        commit('setCurrentClient', response.data);
        return response;
    },
    setCurrentClient({ commit }, client) {
      commit('setCurrentClient', client);
    }
}

const mutations = {
  setClients(state, clients) {
    state.clients = clients;
  },
  setMeta(state, payload) {
    state.meta.total = payload.total;
    state.meta.perPage = payload.per_page;
    state.meta.totalPages = payload.last_page;
    state.meta.currentPage = payload.current_page;
  },
  setCurrentClient(state, payload) {
    state.currentClient = payload
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
