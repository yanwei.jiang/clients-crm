import { fetchClientContacts } from '../../api'

const state = () => ({
  contacts: []
})

const getters = {

}

const actions = {
  async getAllContacts({ commit, dispatch }, clientId) {
    dispatch('general/setIsLoading', {}, {root:true});

    let response = await fetchClientContacts(clientId);
    if (response.status == 200) {
        commit('setContacts', response.data);
    }

    dispatch('general/setIsLoaded', {}, {root:true});
  }
}

const mutations = {
  setContacts(state, payload) {
    state.contacts = payload
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
